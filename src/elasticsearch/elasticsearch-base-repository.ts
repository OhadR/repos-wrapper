import { Client } from "@elastic/elasticsearch";

const proxy = require('proxy-agent');

const logger = require('@log4js-node/log4js-api').getLogger();
logger.category = 'repos-wrapper';


export abstract class EsBaseRepository<T> {

    protected _elasticClient: Client;

    protected abstract getIndex(): string;

    protected constructor() {
        let nodeAgent;
        if(process.env.HTTP_PROXY) {
            nodeAgent = () => proxy(process.env.HTTP_PROXY);
        }

        this._elasticClient = new Client({
            node: process.env.ELASTICSEARCH_URL,
            //log: 'trace',  // <= lets reduce the printouts, default is warning
            auth: {
                username: process.env.ELASTICSEARCH_USERNAME,
                password: process.env.ELASTICSEARCH_PASSWORD
            }
        });
    }

    /**
     * used for REPLACE. UPDATE does not delete fields from the doc, it only updates the given fields.
     */
    public async indexItem(id: string, body: T, index: string = undefined): Promise<string> {
        let response;

        if(!index)
            index = this.getIndex();

        logger.debug(`indexing ${id} into ${index}...`);

        try {
            response = await this._elasticClient.index({
                refresh: true,
                index,
                id,
                //type: 'doc',
                body
            });
        } catch (error) {
            logger.error(error);
            return Promise.reject(error);
        }
        logger.debug(`index response: ${JSON.stringify(response)}`);
        return Promise.resolve(response.body.result);
    }

    public async getItem(id: string): Promise<T> {
        if (!id) {
            return Promise.reject(new Error('getItem() Invalid Args: id is null'));
        }

        let hit;

        try {
            const response = await this._elasticClient.get({
                index: this.getIndex(),
                id,
//                type: 'doc',
            }, {
                ignore: [404]
            });
            logger.trace(response);
            hit = response?.body._source;
        } catch (error) {
            logger.error(error);
            return Promise.reject(error);
        }

        if(hit)
            logger.info(`getItem/${this.getIndex()}: Successfully retrieved item '${id}'`);
        else
            logger.info(`getItem/${this.getIndex()}: item was Not Found '${id}'`);

        return Promise.resolve(hit);
    }

    public async exists(id: string): Promise<boolean> {
        let response;
        try {
            response = await this._elasticClient.exists({
                index: this.getIndex(),
                id,
//                type: 'doc',
            });
        } catch (error) {
            return Promise.reject(error);
        }
        return Promise.resolve(response.body);
    }

    /**
     * used for UPDATE. it only updates the given fields in body.
     */
    public async updateItem(id: string, body: object) {
        let response;
        try {
            response = await this._elasticClient.update({
                index: this.getIndex(),
                id,
//                type: 'doc',
                body: {
                    // put the partial document under the `doc` key
                    doc: body
                }
            });
            logger.debug(response);

        } catch (error) {
            logger.error(error);
            return Promise.reject(error);
        }
        return Promise.resolve(response.body.result);
    }

    public async deleteItem(id: string): Promise<string> {
        let response;
        logger.debug('deleting: ', id);

        try {
            response = await this._elasticClient.delete({
                index: this.getIndex(),
                id,
//                type: 'doc',
            });
        } catch (error) {
            logger.error(error);
            return Promise.reject(error);
        }
        logger.debug('response', response);
        return Promise.resolve(response.body.result);    //result should be 'updated'
    }

    public async search(query: object): Promise<T[]> {
        if (!query) {
            return Promise.reject(new Error('search() Invalid Args: query is null'));
        }

        logger.trace(`search(): Retrieving for query: '${JSON.stringify(query)}'`);
        let items;     //hits from elastic

        try {
            const response = await this._elasticClient.search({
                index: this.getIndex(),
                size: 5000,
                body: query
            });
            items = response?.body.hits?.hits;
        } catch (error) {
            return Promise.reject(error);
        }

        items = items.map((item: { _source: any; }) => item._source);

        logger.debug(`search(): Successfully retrieved ${items.length} items for query`);
        return Promise.resolve(items);
    }


    /**
     * get all items, using scroll
     */
    public async getAllItems(): Promise<T[]> {
        const allQuotes: T[] = [];

        // start things off by searching, setting a scroll timeout, and pushing
        // our first response into the queue to be processed
        let response = await this._elasticClient.search({
            index: this.getIndex(),
            // keep the search results "scrollable" for 30 seconds
            scroll: '30s',
            size: 1000,
            // filter the source to only include the quote field
//            _source: ['quote'],
            body: {
                query: {
                    match_all: {}
                }
            }
        });


        while (true) {

            logger.debug('$$$$$$$$$$$$$$ ' + response.body.hits.hits.length);
            if(response.body.hits.hits.length == 0) {
                break;
            }

            // collect the titles from this response
            response.body.hits.hits.forEach(function (hit: any) {
                //debug(hit._id);
                allQuotes.push(hit._source)
            });

            // get the next response if there are more quotes to fetch
            response = await this._elasticClient.scroll({
                scroll_id: response.body._scroll_id,
                scroll: '30s'
            });
        }

        return Promise.resolve(allQuotes);
    }

    /**
     *
     * @param events The operation definition and data (action-data pairs), separated by newlines
     * @param type
     */
    public async bulkInsert(events: T[], type: string = 'doc'): Promise<void> {
        const body = events.flatMap(doc => [
            {
                index: {
                    _index: this.getIndex(),
                    _type: type
                }
            },
            doc]);
        console.log(body);

        try {
            const bulkResponse = await this._elasticClient.bulk({refresh: true, timeout: '3m', wait_for_active_shards: '1', body});
            //logger.info(bulkResponse);
            logger.info(`bulkInsert: INSERTED ${bulkResponse.body.items.length} items into index ${this.getIndex()}`);
        } catch (error) {
            logger.error(error);
            return Promise.reject(error);
        }
    }

}