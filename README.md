# Repos Wrapper Library

## Build and Deploy

    npm install
    npm run build
    npm version patch
    npm publish
    git push

note that `npm run build` invokes `prebuild` script, to clean dist folder.


## elasticsearch js client

`elasticsearch` is the "old" elasticsearch js client, deployed in version 1.x.x.

`@elastic/elasticsearch` is the ["new" elasticsearch js client](https://www.elastic.co/blog/new-elasticsearch-javascript-client-released), 
and 2 versions supported (note there are differences!!):

* `@elastic/elasticsearch@6` on branch main, deployed to npm in version 6.8.x

* `@elastic/elasticsearch@7` on branch `new-es-js-client-7.x`, deployed to npm in version 2.x.x.


### installations:

    npm install @elastic/elasticsearch@6 
    npm install @elastic/elasticsearch@7



## logs

This library uses log4js. However, rather than being dependent on a *specific version* of `log4js`, in order to avoid 
dependencies headache for this library users, it is dependent on `@log4js-node/log4js-api`


## Tests - TBD

### Elastic

## versions

### 7.2.2

S3 improvements. prefer log4js-api, copyFolder and deleteFolder.

### 7.3.0

remove S3 after split to [repos-wrapper-s3](https://gitlab.com/OhadR/s3-wrapper)
