import { EsBaseRepository } from "../src/elasticsearch/elasticsearch-base-repository";
require('./log-config');

const TEST_INDEX = 'ohads-5';

interface TestObj {
    //indices:
    assetId: string;

    jobId: string;
    status: string;
    createTime: string;
}

function createTestObj(): TestObj {
    const num = Math.round(Math.random() * 10000);
    return {
        assetId: num+'', createTime: "", jobId: "YYY", status: "OK"
    }
}

function createTestObjBulk(bulkSize: number): TestObj[] {

    const bulk = [];
    for(let i = 0; i < bulkSize; ++i) {
        const num = Math.round(Math.random() * 10000);
        bulk.push({
            assetId: num + '', createTime: "", jobId: "YYY", status: "OK"
        });
    }
    return bulk;
}

export class TesttEsRepository extends EsBaseRepository<TestObj> {

    private static _instance: TesttEsRepository;

    constructor() {
        super();
    }

    public static get instance() {
        if( !TesttEsRepository._instance )
            TesttEsRepository._instance = new TesttEsRepository();

        return TesttEsRepository._instance;
    }

    protected getIndex(): string {
        return TEST_INDEX;
    }
}


const ownerIdQuery =  {
    query: {
        term: { jobId: 'YYY' }
    }
};

const id = '758';

async function run() {
    const response_ = await TesttEsRepository.instance.indexItem(id, createTestObj());
    console.log(response_);

    const response = await TesttEsRepository.instance.getItem(id);
    console.log(response);

    const response2 = await TesttEsRepository.instance.exists(id);
    console.log(response2);

    const response3 = await TesttEsRepository.instance.search(ownerIdQuery);
    console.log(response3);

    await TesttEsRepository.instance.bulkInsert(createTestObjBulk(110));
}

run();